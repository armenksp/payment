<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use JetBrains\PhpStorm\NoReturn;
use Srmklive\PayPal\Services\ExpressCheckout;

class PayPalPaymentController extends Controller
{
    /**
     * @throws Exception
     */
    #[NoReturn] public function handlePayment(): Redirector|Application|RedirectResponse
    {
        $product = [];
        $product['items'] = [
            [
                'name' => 'Nike Joyride 2',
                'price' => 1,
                'desc' => 'Running shoes for Men',
                'qty' => 2
            ]
        ];


        $product['invoice_id'] = 1;
        $product['invoice_description'] = "Order #{$product['invoice_id']} Bill";
        $product['return_url'] = route('success.payment');
        $product['cancel_url'] = route('cancel.payment');
        $product['total'] = 2;


        $paypalModule = new ExpressCheckout;
        $res = $paypalModule->setExpressCheckout($product, true);

        return redirect($res['paypal_link']);
    }

    #[NoReturn] public function paymentCancel()
    {
        dd('Your payment has been declension. The payment cancellation page goes here!');
    }

    #[NoReturn] public function paymentSuccess(Request $request)
    {
        $paypalModule = new ExpressCheckout;
        $response = $paypalModule->getExpressCheckoutDetails($request->token);

        if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSFULNESSES'])) {
            dd('Payment was successfully. The payment success page goes here!');
        }

        dd('Error occurred!');
    }
}
